#!/usr/bin/env bash

# -------------------------------------------------------------------------------------------------------------------- #
# GitLab API. Create project.
# -------------------------------------------------------------------------------------------------------------------- #
# @author Kitsune Solar <kitsune.solar@gmail.com>
# @version 1.0.0
# -------------------------------------------------------------------------------------------------------------------- #

curl=$( which curl )
api_ver="4"
sleep="2"

OPTIND=1

while getopts "t:n:v:s:g:h" opt; do
    case ${opt} in
        t)
            token="${OPTARG}"
            ;;
        n)
            project_name="${OPTARG}"; IFS=';' read -a project_name <<< "${project_name}"
            ;;
        v)
            project_visibility="${OPTARG}"
            ;;
        s)
            project_tags="${OPTARG}"
            ;;
        g)
            project_namespace="${OPTARG}"
            ;;
        h|*)
            echo "-t [token] -n [project_name] -v [project_visibility] -s [project_tags] -g [project_namespace]"
            exit 2
            ;;
        \?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
    esac
done

shift $(( ${OPTIND} - 1 ))

if (( ! ${#project_name[@]} )) || [[ -z "${project_namespace}" ]]; then exit 1; fi

case ${project_visibility} in
    private)
        project_visibility="private"
        ;;
    internal)
        project_visibility="internal"
        ;;
    public)
        project_visibility="public"
        ;;
    *)
        exit 1
        ;;
esac

for i in "${project_name[@]}"; do
    project_url="$( echo ${i} | tr '[:upper:]' '[:lower:]' )"

    echo ""
    echo "--- Open: ${i}"

    ${curl}                             \
    --header "PRIVATE-TOKEN: ${token}"  \
    --request POST                      \
    "https://gitlab.com/api/v${api_ver}/projects?name=${i}&path=${project_url}&namespace_id=${project_namespace}&visibility=${project_visibility}&tag_list=${project_tags}&initialize_with_readme=true"

    echo ""
    echo "--- Done: ${i}"
    echo ""

    sleep ${sleep}
done

exit 0
