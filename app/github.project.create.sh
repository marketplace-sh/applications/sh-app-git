#!/usr/bin/env bash

# -------------------------------------------------------------------------------------------------------------------- #
# GitHub API. Create project.
# -------------------------------------------------------------------------------------------------------------------- #
# @author Kitsune Solar <kitsune.solar@gmail.com>
# @version 1.0.0
# -------------------------------------------------------------------------------------------------------------------- #

curl=$( which curl )
sleep="2"

OPTIND=1

while getopts "t:n:d:x:o:ipwh" opt; do
    case ${opt} in
        t)
            token="${OPTARG}"
            ;;
        n)
            project_name="${OPTARG}"; IFS=';' read -a project_name <<< "${project_name}"
            ;;
        d)
            project_description="${OPTARG}"
            ;;
        x)
            project_homepage="${OPTARG}"
            ;;
        o)
            project_org="${OPTARG}"
            ;;
        i)
            set_issues=1
            ;;
        p)
            set_projects=1
            ;;
        w)
            set_wiki=1
            ;;
        h|*)
            echo "-t [token] -n [project_name] -d [project_description] -x [project_homepage] -o [project_org] -i -p -w"
            exit 2
            ;;
        \?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
    esac
done

shift $(( ${OPTIND} - 1 ))

if (( ! ${#project_name[@]} )) || [[ -z "${project_org}" ]]; then exit 1; fi

[[ -n "${set_issues}" ]] && has_issues="true" || has_issues="false"
[[ -n "${set_projects}" ]] && has_projects="true" || has_projects="false"
[[ -n "${set_wiki}" ]] && has_wiki="true" || has_wiki="false"

for i in "${project_name[@]}"; do
    echo ""
    echo "--- Open: ${i}"

    ${curl}                                             \
    --header "Authorization: token ${token}"            \
    --header "Content-Type: application/json"           \
    --request POST                                      \
    "https://api.github.com/orgs/${project_org}/repos"  \
    --data @- << EOF
{
    "name": "${i}",
    "description": "${project_description}",
    "homepage": "${project_homepage}",
    "has_issues": ${has_issues},
    "has_projects": ${has_projects},
    "has_wiki": ${has_wiki}
}
EOF

    echo ""
    echo "--- Done: ${i}"
    echo ""

    sleep ${sleep}
done

exit 0
