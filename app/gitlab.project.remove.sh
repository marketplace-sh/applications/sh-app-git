#!/usr/bin/env bash

# -------------------------------------------------------------------------------------------------------------------- #
# GitLab API. Remove project.
# -------------------------------------------------------------------------------------------------------------------- #
# @author Kitsune Solar <kitsune.solar@gmail.com>
# @version 1.0.0
# -------------------------------------------------------------------------------------------------------------------- #

curl=$( which curl )
api_ver="4"
sleep="2"

OPTIND=1

while getopts "t:i:h" opt; do
    case ${opt} in
        t)
            token="${OPTARG}"
            ;;
        i)
            project_id="${OPTARG}"; IFS=';' read -a project_id <<< "${project_id}"
            ;;
        h|*)
            echo "-t [token] -i [group_id]"
            exit 2
            ;;
        \?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
    esac
done

shift $((${OPTIND} - 1))

if (( ! ${#project_id[@]} )); then exit 1; fi

for i in "${project_id[@]}"; do
    echo ""
    echo "--- Remove: ${i}"

    ${curl}                             \
    --header "PRIVATE-TOKEN: ${token}"  \
    --request DELETE                    \
    "https://gitlab.com/api/v${api_ver}/projects/${i}"

    echo ""
    echo "--- Done: ${i}"
    echo ""

    sleep ${sleep}
done

exit 0
