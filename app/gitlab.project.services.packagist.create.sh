#!/usr/bin/env bash

# -------------------------------------------------------------------------------------------------------------------- #
# GitLab API. Create Packagist.
# -------------------------------------------------------------------------------------------------------------------- #
# @author Kitsune Solar <kitsune.solar@gmail.com>
# @version 1.0.0
# -------------------------------------------------------------------------------------------------------------------- #

curl=$( which curl )
api_ver="4"
sleep="2"

OPTIND=1

while getopts "t:u:k:i:h" opt; do
    case ${opt} in
        t)
            token="${OPTARG}"
            ;;
        u)
            service_username="${OPTARG}"
            ;;
        k)
            service_token="${OPTARG}"
            ;;
        i)
            project_id="${OPTARG}"; IFS=';' read -a project_id <<< "${project_id}"
            ;;
        h|*)
            echo "-t [token] -u [service_username] -k [service_token] -i [project_id]"
            exit 2
            ;;
        \?)
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
    esac
done

shift $((${OPTIND} - 1))

if (( ! ${#project_id[@]} )) || [[ -z "${service_username}" ]]; then exit 1; fi

for i in "${project_id[@]}"; do
    echo ""
    echo "--- Edit: ${i}"

    ${curl}                             \
    --header "PRIVATE-TOKEN: ${token}"  \
    --request PUT                       \
    "https://gitlab.com/api/v${api_ver}/projects/${i}/services/packagist?username=${service_username}&token=${service_token}"

    echo ""
    echo "--- Done: ${i}"
    echo ""

    sleep ${sleep}
done

exit 0
